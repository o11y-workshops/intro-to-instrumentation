FROM python:3.13-bullseye

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

RUN pip install prometheus-flask-exporter

COPY . .

CMD [ "flask", "run", "--host=0.0.0.0"]
